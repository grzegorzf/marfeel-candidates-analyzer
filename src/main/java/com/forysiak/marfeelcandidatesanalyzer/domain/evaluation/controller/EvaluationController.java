package com.forysiak.marfeelcandidatesanalyzer.domain.evaluation.controller;

import com.forysiak.marfeelcandidatesanalyzer.domain.evaluation.model.EvaluationTaskDocument;
import com.forysiak.marfeelcandidatesanalyzer.domain.evaluation.repository.EvaluationRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@Slf4j
@RequestMapping(EvaluationApi.BASE_EVALUATION_PATH)
@AllArgsConstructor
public class EvaluationController {

  private final EvaluationRepository evaluationRepository;

  @GetMapping
  public List<EvaluationTaskDocument> getAllEvaluationTasks() {
    List<EvaluationTaskDocument> evaluationTaskDocuments = evaluationRepository.findAll();
    log.info("All EvaluationTaskDocuments returned, total length: {}", evaluationTaskDocuments.size());
    return evaluationTaskDocuments;
  }
}
