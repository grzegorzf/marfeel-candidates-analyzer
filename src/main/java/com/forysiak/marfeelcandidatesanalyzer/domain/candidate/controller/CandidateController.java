package com.forysiak.marfeelcandidatesanalyzer.domain.candidate.controller;

import com.forysiak.marfeelcandidatesanalyzer.domain.candidate.model.CandidateProposition;
import com.forysiak.marfeelcandidatesanalyzer.domain.candidate.model.CandidateDocument;
import com.forysiak.marfeelcandidatesanalyzer.domain.candidate.model.Qualification;
import com.forysiak.marfeelcandidatesanalyzer.domain.candidate.repository.CandidateRepository;
import com.forysiak.marfeelcandidatesanalyzer.domain.candidate.service.candidate_service.CandidateService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import static com.forysiak.marfeelcandidatesanalyzer.domain.candidate.model.Qualification.INVALID;
import static com.forysiak.marfeelcandidatesanalyzer.domain.candidate.model.Qualification.NEGATIVE;
import static com.forysiak.marfeelcandidatesanalyzer.domain.candidate.model.Qualification.POSITIVE;

@RestController
@RequestMapping(CandidateApi.BASE_API_PATH)
@AllArgsConstructor
@Slf4j
public class CandidateController {

  private final CandidateService candidateService;
  private final CandidateRepository candidateRepository;

  @PostMapping(CandidateApi.BASE_CANDIDATE_PATH)
  public ResponseEntity<Void> evaluateCandidates(@Valid @RequestBody CandidateProposition[] candidatePropositions) {
    log.info("Started evaluation of candidatePropositions, amount to process: {}", candidatePropositions.length);
    candidateService.evaluatePropositions(candidatePropositions);
    return new ResponseEntity<>(HttpStatus.OK);
  }

  @GetMapping(CandidateApi.BASE_CANDIDATE_PATH)
  public List<CandidateDocument> findAllCandidates() {
    List<CandidateDocument> candidateDocuments = candidateService.findAll();
    log.info("All CandidateDocuments returned, total length: {}", candidateDocuments.size());
    return candidateDocuments;
  }

  @GetMapping(CandidateApi.BASE_CANDIDATE_PATH + "/{qualification}")
  public List<CandidateDocument> findSpecificCandidates(@PathVariable("qualification") Qualification qualification) {
    return candidateService.findAllByQualification(qualification);
  }

  @GetMapping(CandidateApi.POSITIVE_CANDIDATES_PATH)
  public List<CandidateDocument> findPositiveCandidates() {
    List<CandidateDocument> positiveCandidateDocuments = candidateService.findAllByQualification(POSITIVE);
    log.info("All positive CandidateDocuments, total length: {}", positiveCandidateDocuments.size());
    return positiveCandidateDocuments;
  }

  @GetMapping(CandidateApi.NEGATIVE_CANDIDATES_PATH)
  public List<CandidateDocument> findNegativeCandidates() {
    List<CandidateDocument> negativeCandidateDocuments = candidateService.findAllByQualification(NEGATIVE);
    log.info("All negative CandidateDocuments, total length: {}", negativeCandidateDocuments.size());
    return negativeCandidateDocuments;
  }

  @GetMapping(CandidateApi.INVALID_CANDIDATES_PATH)
  public List<CandidateDocument> findInvalidCandidates() {
    List<CandidateDocument> invalidCandidateDocuments = candidateService.findAllByQualification(INVALID);
    log.info("All invalid CandidateDocuments, total length: {}", invalidCandidateDocuments.size());
    return invalidCandidateDocuments;
  }

  @DeleteMapping(CandidateApi.BASE_CANDIDATE_PATH)
  public ResponseEntity<Void> deleteAllCandidates() {
    log.info("Deleted all CandidateDocuments.");
    candidateRepository.deleteAll();
    return new ResponseEntity<>(HttpStatus.OK);
  }

}
