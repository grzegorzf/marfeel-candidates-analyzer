package com.forysiak.marfeelcandidatesanalyzer.domain.candidate.service.candidate_service;

import com.forysiak.marfeelcandidatesanalyzer.domain.candidate.model.CandidateDocument;
import com.forysiak.marfeelcandidatesanalyzer.domain.candidate.model.CandidateProposition;
import com.forysiak.marfeelcandidatesanalyzer.domain.candidate.model.Qualification;

import java.util.List;

public interface CandidateService {

  void evaluatePropositions(CandidateProposition[] candidatePropositions);

  CandidateDocument evaluateProposition(CandidateProposition candidateProposition);

  CandidateDocument persistProposition(CandidateProposition candidateProposition);

  CandidateDocument updateCandidateIfNecessary(CandidateProposition candidateProposition, CandidateDocument candidateDocument);

  List<CandidateDocument> findAll();

  List<CandidateDocument> findAllByQualification(Qualification qualification);

}
