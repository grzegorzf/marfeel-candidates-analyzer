package com.forysiak.marfeelcandidatesanalyzer.domain.evaluation.service;

import com.forysiak.marfeelcandidatesanalyzer.domain.candidate.model.CandidateDocument;

import java.util.List;
import java.util.concurrent.Future;

public interface EvaluationService {
  void persistEvaluationTask(List<Future<CandidateDocument>> completable, long startProcessingTime);
}
