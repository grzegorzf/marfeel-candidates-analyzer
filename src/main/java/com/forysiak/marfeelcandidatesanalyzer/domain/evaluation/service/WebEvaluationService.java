package com.forysiak.marfeelcandidatesanalyzer.domain.evaluation.service;

import com.forysiak.marfeelcandidatesanalyzer.domain.candidate.function.CandidatesToInvalidCandidates;
import com.forysiak.marfeelcandidatesanalyzer.domain.candidate.function.CandidatesToNegativeCandidates;
import com.forysiak.marfeelcandidatesanalyzer.domain.candidate.function.CandidatesToPositiveCandidates;
import com.forysiak.marfeelcandidatesanalyzer.domain.candidate.model.CandidateDocument;
import com.forysiak.marfeelcandidatesanalyzer.domain.evaluation.function.FutureToCandidateDocument;
import com.forysiak.marfeelcandidatesanalyzer.domain.evaluation.model.EvaluationTaskDocument;
import com.forysiak.marfeelcandidatesanalyzer.domain.evaluation.repository.EvaluationRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

@Service
@Slf4j
@AllArgsConstructor
public class WebEvaluationService implements EvaluationService {

  private final EvaluationRepository evaluationRepository;
  private final FutureToCandidateDocument futureToCandidateDocument;
  private final CandidatesToPositiveCandidates candidatesToPositiveCandidates;
  private final CandidatesToNegativeCandidates candidatesToNegativeCandidates;
  private final CandidatesToInvalidCandidates candidatesToInvalidCandidates;

  @Override
  public void persistEvaluationTask(List<Future<CandidateDocument>> completable, long startProcessingTime) {

    List<CandidateDocument> processedCandidates = completable.stream()
        .map(futureToCandidateDocument)
        .filter(Objects::nonNull)
        .collect(Collectors.toList());

    long processingTime = System.currentTimeMillis() - startProcessingTime;
    List<CandidateDocument> positiveCandidates = candidatesToPositiveCandidates.apply(processedCandidates);
    List<CandidateDocument> negativeCandidates = candidatesToNegativeCandidates.apply(processedCandidates);
    List<CandidateDocument> invalidCandidates = candidatesToInvalidCandidates.apply(processedCandidates);

    EvaluationTaskDocument evaluationTaskDocument = EvaluationTaskDocument.builder()
        .evaluatedAt(LocalDate.now())
        .processingTime(processingTime)
        .positiveCandidates(positiveCandidates)
        .negativeCandidates(negativeCandidates)
        .invalidCandidates(invalidCandidates)
        .build();

    log.info("EvaluationTask finished in {}", processingTime);
    evaluationRepository.save(evaluationTaskDocument);
  }
}
