package com.forysiak.marfeelcandidatesanalyzer.domain.candidate.predicate;

import com.forysiak.marfeelcandidatesanalyzer.domain.candidate.model.CandidateProposition;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.function.Predicate;
import java.util.regex.Pattern;

@Component
@Slf4j
public class HasValidURL implements Predicate<CandidateProposition> {

  private static final Pattern VALID_URL_REGEX  = Pattern.compile("^(?:https?:\\/\\/)?(?:www\\.)?[a-zA-Z0-9./-]+$");

  @Override
  public boolean test(CandidateProposition candidateProposition) {
    boolean isValid = VALID_URL_REGEX.matcher(candidateProposition.getUrl()).find();
    if (!isValid) {
      log.warn("Encountered invalid URL: {} for CandidateProposition: {}", candidateProposition.getUrl(), candidateProposition);
    }
    return isValid;
  }
}
