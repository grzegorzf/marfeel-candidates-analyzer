package com.forysiak.marfeelcandidatesanalyzer.domain.candidate.model;

import java.io.Serializable;

public enum Qualification implements Serializable {
  POSITIVE,
  NEGATIVE,
  INVALID
}
