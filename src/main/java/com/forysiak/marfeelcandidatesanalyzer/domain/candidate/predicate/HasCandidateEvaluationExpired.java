package com.forysiak.marfeelcandidatesanalyzer.domain.candidate.predicate;

import com.forysiak.marfeelcandidatesanalyzer.domain.candidate.model.CandidateDocument;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.function.Predicate;

@Component
@Slf4j
public class HasCandidateEvaluationExpired implements Predicate<CandidateDocument> {

  private static final int EXPIRATION_TIME_IN_WEEKS = 2;

  @Override
  public boolean test(CandidateDocument candidateDocument) {
    LocalDate expirationDate = LocalDate.now().minusWeeks(EXPIRATION_TIME_IN_WEEKS);
    boolean evaluationExpired = candidateDocument.getEvaluatedAt().isBefore(expirationDate);
    if (evaluationExpired) {
      log.warn("EvaluationExpired for CandidateProposition: {}", candidateDocument);
    }
    return evaluationExpired;
  }
}
