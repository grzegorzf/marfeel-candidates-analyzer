package com.forysiak.marfeelcandidatesanalyzer.domain.candidate.predicate;

import com.forysiak.marfeelcandidatesanalyzer.domain.candidate.qualifier.MarfeelizableByTitle;
import org.jsoup.nodes.Document;
import org.springframework.stereotype.Component;

import java.util.regex.Pattern;

@Component
@MarfeelizableByTitle
public class IsDocumentMarfeelizableBasedOnTitle implements IsDocumentMarfeelizable {

  private static final  String TITLE = "title";
  private static final Pattern REQUESTED_PATTERN = Pattern.compile("(?:news|noticas)");

  @Override
  public boolean test(Document document) {
    String titleText = document.select(TITLE).text();
    return REQUESTED_PATTERN.matcher(titleText).find();
  }
}
