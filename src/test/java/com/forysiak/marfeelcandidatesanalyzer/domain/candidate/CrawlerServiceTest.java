package com.forysiak.marfeelcandidatesanalyzer.domain.candidate;

import com.forysiak.marfeelcandidatesanalyzer.MainApplication;
import com.forysiak.marfeelcandidatesanalyzer.domain.candidate.function.UrlToFullUrl;
import com.forysiak.marfeelcandidatesanalyzer.domain.candidate.predicate.IsDocumentMarfeelizableBasedOnTitle;
import org.jsoup.nodes.Document;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static com.forysiak.marfeelcandidatesanalyzer.domain.candidate.CandidateContentGenerator.generateDocumentWithPositiveQualificationByTitle;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = MainApplication.class)
public class CrawlerServiceTest {

  @Autowired
  private IsDocumentMarfeelizableBasedOnTitle marfeelizableBasedOnTitle;

  @Autowired
  private UrlToFullUrl urlToFullUrl;

  @Test
  public void should_properly_qualify_document() {
    // given
    Document positiveDocument = generateDocumentWithPositiveQualificationByTitle();
    Document negativeDocument = CandidateContentGenerator.generateDocumentWithNegativeQualificationByTitle();
    // when
    boolean shouldBeMarfeelizable = marfeelizableBasedOnTitle.test(positiveDocument);
    boolean ShouldNotBeMarfeelizable = !marfeelizableBasedOnTitle.test(negativeDocument);
    //then
    assertThat(shouldBeMarfeelizable).isTrue();
    assertThat(ShouldNotBeMarfeelizable).isTrue();
  }

  @Test
  public void should_properly_append_url(){
    // given
    String url = "google.com";
    // when
    String fullUrl = urlToFullUrl.apply(url);
    // then
    assertThat(fullUrl).isEqualTo("https://google.com");
  }

}
