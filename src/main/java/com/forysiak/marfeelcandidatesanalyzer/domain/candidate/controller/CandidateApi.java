package com.forysiak.marfeelcandidatesanalyzer.domain.candidate.controller;

public interface CandidateApi {
  String BASE_API_PATH = "/api";
  String BASE_CANDIDATE_PATH = "/candidates";
  String POSITIVE_CANDIDATES_PATH = "/positive-candidates";
  String NEGATIVE_CANDIDATES_PATH = "/negative-candidates";
  String INVALID_CANDIDATES_PATH = "/invalid-candidates";
}