package com.forysiak.marfeelcandidatesanalyzer.domain.candidate.predicate;

import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;

@Component
public class DistinctByKey  {
  public <T> Predicate<T> test(Function<? super T, Object> keyExtractor){
    Map<Object, Boolean> map = new ConcurrentHashMap<>();
    return t -> map.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
  }
}


