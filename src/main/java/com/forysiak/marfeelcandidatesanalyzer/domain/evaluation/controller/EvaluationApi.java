package com.forysiak.marfeelcandidatesanalyzer.domain.evaluation.controller;

public interface EvaluationApi {
  String BASE_EVALUATION_PATH = "/api/evaluation";
}
