package com.forysiak.marfeelcandidatesanalyzer.configs;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import({WebConfig.class, SwaggerConfig.class})
public class AppConfig {

}
