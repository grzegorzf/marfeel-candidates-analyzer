package com.forysiak.marfeelcandidatesanalyzer.domain.candidate.model;

import lombok.*;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Id;
import javax.persistence.Transient;
import java.time.Clock;
import java.time.LocalDate;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
@Document(collection = "Candidate")
public class CandidateDocument {

  public CandidateDocument(String url, long rank, Qualification qualification) {
    this.url = url;
    this.rank = rank;
    this.qualification = qualification;
    this.evaluatedAt = LocalDate.now(Clock.systemUTC());
  }

  public CandidateDocument(String url, long rank, Qualification qualification, LocalDate evaluatedAt) {
    this(url, rank, qualification);
    this.evaluatedAt = evaluatedAt;
  }

  @Id
  private ObjectId id = new ObjectId();

  @Indexed
  private String url;

  private long rank;

  private Qualification qualification;

  private boolean customer = false;

  private LocalDate evaluatedAt;

  @Transient
  public void changeQualificationAndRank(Qualification qualification, long rank){
    this.qualification = qualification;
    this.rank = rank;
    this.evaluatedAt = LocalDate.now();
  }

}
