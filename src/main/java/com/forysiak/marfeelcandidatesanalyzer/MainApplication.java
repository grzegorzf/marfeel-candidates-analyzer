package com.forysiak.marfeelcandidatesanalyzer;

import com.forysiak.marfeelcandidatesanalyzer.configs.AppConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;

@EnableAutoConfiguration(exclude = {DataSourceAutoConfiguration.class})
@ComponentScan
@Import(AppConfig.class)
public class MainApplication {
  public static void main(String[] args) {
    AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
    context.register(AppConfig.class);
		SpringApplication.run(MainApplication.class, args);
	}
}
