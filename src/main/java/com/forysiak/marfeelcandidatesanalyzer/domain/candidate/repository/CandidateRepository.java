package com.forysiak.marfeelcandidatesanalyzer.domain.candidate.repository;

import com.forysiak.marfeelcandidatesanalyzer.domain.candidate.model.CandidateDocument;
import com.forysiak.marfeelcandidatesanalyzer.domain.candidate.model.Qualification;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CandidateRepository extends MongoRepository<CandidateDocument, String> {

  List<CandidateDocument> findAllByQualification(Qualification qualification);

  Optional<CandidateDocument> findByUrl(String url);
}
