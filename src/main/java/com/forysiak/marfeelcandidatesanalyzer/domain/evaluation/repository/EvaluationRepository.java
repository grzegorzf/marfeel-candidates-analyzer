package com.forysiak.marfeelcandidatesanalyzer.domain.evaluation.repository;

import com.forysiak.marfeelcandidatesanalyzer.domain.evaluation.model.EvaluationTaskDocument;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EvaluationRepository extends MongoRepository<EvaluationTaskDocument, String> {

}
