package com.forysiak.marfeelcandidatesanalyzer.domain.candidate.qualifier;

@MarfeelizablePredicate
public @interface MarfeelizableByTitle {
}
