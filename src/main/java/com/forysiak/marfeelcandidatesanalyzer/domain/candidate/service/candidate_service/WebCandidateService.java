package com.forysiak.marfeelcandidatesanalyzer.domain.candidate.service.candidate_service;

import com.forysiak.marfeelcandidatesanalyzer.domain.candidate.model.CandidateDocument;
import com.forysiak.marfeelcandidatesanalyzer.domain.candidate.model.CandidateProposition;
import com.forysiak.marfeelcandidatesanalyzer.domain.candidate.model.Qualification;
import com.forysiak.marfeelcandidatesanalyzer.domain.candidate.predicate.DistinctByKey;
import com.forysiak.marfeelcandidatesanalyzer.domain.candidate.predicate.HasCandidateEvaluationExpired;
import com.forysiak.marfeelcandidatesanalyzer.domain.candidate.predicate.HasValidURL;
import com.forysiak.marfeelcandidatesanalyzer.domain.candidate.repository.CandidateRepository;
import com.forysiak.marfeelcandidatesanalyzer.domain.candidate.service.crawler_service.CrawlerService;
import com.forysiak.marfeelcandidatesanalyzer.domain.evaluation.service.EvaluationService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.*;
import java.util.stream.Collectors;

@AllArgsConstructor
@Service
public class WebCandidateService implements CandidateService {

  private final CrawlerService crawlerService;
  private final EvaluationService evaluationService;
  private final CandidateRepository candidateRepository;
  private final HasValidURL hasValidURL;
  private final DistinctByKey distinctByKey;
  private final HasCandidateEvaluationExpired hasCandidateEvaluationExpired;

  private final ExecutorService executorService = Executors.newFixedThreadPool(20);
  private final CompletionService<CandidateDocument> completionService = new ExecutorCompletionService<>(executorService);

  @Override
  public void evaluatePropositions(CandidateProposition[] propositions){

    long startProcessingTime = System.currentTimeMillis();

    List<Future<CandidateDocument>> completable = Arrays.stream(propositions)
        .filter(hasValidURL)
        .filter(distinctByKey.test(CandidateProposition::getUrl))
        .map(proposition -> completionService.submit(() -> evaluateProposition(proposition)))
        .collect(Collectors.toList());

    evaluationService.persistEvaluationTask(completable, startProcessingTime);

  }

  @Override
  public CandidateDocument evaluateProposition(CandidateProposition proposition) {
    String url = proposition.getUrl();
    Optional<CandidateDocument> existing = candidateRepository.findByUrl(url);
    if (existing.isPresent())
      return updateCandidateIfNecessary(proposition, existing.get());
    else
      return persistProposition(proposition);
  }

  @Override
  public synchronized CandidateDocument persistProposition(CandidateProposition proposition) {
    Qualification qualification = crawlerService.calculateQualification(proposition);
    CandidateDocument candidate = new CandidateDocument(proposition.getUrl(), proposition.getRank(), qualification);
    return candidateRepository.save(candidate);
  }

  @Override
  public synchronized CandidateDocument updateCandidateIfNecessary(CandidateProposition proposition, CandidateDocument candidateDocument) {
    if (hasCandidateEvaluationExpired.test(candidateDocument)){
      Qualification qualification = crawlerService.calculateQualification(proposition);
      candidateDocument.changeQualificationAndRank(qualification, proposition.getRank());
      return candidateRepository.save(candidateDocument);
    }
    return candidateDocument;
  }

  @Override
  public List<CandidateDocument> findAll() {
    return candidateRepository.findAll();
  }

  @Override
  public List<CandidateDocument> findAllByQualification(Qualification qualification) {
    return candidateRepository.findAllByQualification(qualification);
  }

}
