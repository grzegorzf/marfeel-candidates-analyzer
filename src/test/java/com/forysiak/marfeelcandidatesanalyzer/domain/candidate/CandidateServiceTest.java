package com.forysiak.marfeelcandidatesanalyzer.domain.candidate;

import com.forysiak.marfeelcandidatesanalyzer.MainApplication;
import com.forysiak.marfeelcandidatesanalyzer.domain.candidate.model.CandidateDocument;
import com.forysiak.marfeelcandidatesanalyzer.domain.candidate.model.CandidateProposition;
import com.forysiak.marfeelcandidatesanalyzer.domain.candidate.model.Qualification;
import com.forysiak.marfeelcandidatesanalyzer.domain.candidate.predicate.HasCandidateEvaluationExpired;
import com.forysiak.marfeelcandidatesanalyzer.domain.candidate.predicate.HasValidURL;
import com.forysiak.marfeelcandidatesanalyzer.domain.candidate.repository.CandidateRepository;
import com.forysiak.marfeelcandidatesanalyzer.domain.candidate.service.candidate_service.CandidateService;
import com.forysiak.marfeelcandidatesanalyzer.domain.evaluation.model.EvaluationTaskDocument;
import com.forysiak.marfeelcandidatesanalyzer.domain.evaluation.repository.EvaluationRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = MainApplication.class)
public class CandidateServiceTest {

  @Autowired
  private CandidateRepository candidateRepository;

  @Autowired
  private EvaluationRepository evaluationRepository;

  @Autowired
  private CandidateService candidateService;

  @Autowired
  private HasCandidateEvaluationExpired hasCandidateEvaluationExpired;
  @Autowired
  private HasValidURL hasValidURL;


  @Before
  public final void setUp() {
    candidateRepository.deleteAll();
    evaluationRepository.deleteAll();
    CandidateDocument expiredInitialCandidate = new CandidateDocument("shinjuku.com", 3, Qualification.POSITIVE, LocalDate.now().minusWeeks(3));
    CandidateDocument nonExpiredInitialCandidate = new CandidateDocument("akihabara.com", 2, Qualification.NEGATIVE, LocalDate.now().minusWeeks(1));
    candidateRepository.save(nonExpiredInitialCandidate);
    candidateRepository.save(expiredInitialCandidate);
  }

  @After
  public final void tearDown() {
    candidateRepository.deleteAll();
    evaluationRepository.deleteAll();
  }

  @Test
  public void should_properly_check_evaluation_expiration(){
    // given
    CandidateDocument candidateWithExpiredEvaluation = new CandidateDocument("apples.com", 1, Qualification.POSITIVE, LocalDate.now().minusWeeks(3));
    CandidateDocument candidateWithNonExpiredEvaluation = new CandidateDocument("apple.com", 1, Qualification.POSITIVE, LocalDate.now().minusWeeks(1));

    // when
    boolean shouldHaveExpiredEvaluation = hasCandidateEvaluationExpired.test(candidateWithExpiredEvaluation);
    boolean shouldHaveNotExpiredEvaluation = !hasCandidateEvaluationExpired.test(candidateWithNonExpiredEvaluation);

    // then
    assertThat(shouldHaveExpiredEvaluation).isTrue();
    assertThat(shouldHaveNotExpiredEvaluation).isTrue();
  }

  @Test
  public void should_properly_accept_or_reject_proposition_basing_on_url(){
    // given
    CandidateProposition propositionWithNiceUrl = new CandidateProposition("mi-6.co.uk", 1);
    CandidateProposition propositionWithBrokenUrl = new CandidateProposition("clearly broken url...", 1);

    // when
    boolean shouldBeAccepted = hasValidURL.test(propositionWithNiceUrl);
    boolean shouldBeRejected = !hasValidURL.test(propositionWithBrokenUrl);

    // then
    assertThat(shouldBeAccepted).isTrue();
    assertThat(shouldBeRejected).isTrue();
  }

  @Test
  public void should_properly_evaluate_update_persist_or_ignore(){
    // given
    CandidateProposition newShouldBePersistedProposition1 = new CandidateProposition("canon.com", 342);
    CandidateProposition newShouldBePersistedProposition2 = new CandidateProposition("nikon.com", 3);
    CandidateProposition existingShouldBeUpdatedProposition = new CandidateProposition("shinjuku.com", 6);
    CandidateProposition existingShouldBeIgnoredProposition = new CandidateProposition("akihabara.com", 8);
    CandidateProposition newShouldBeIgnoredProposition = new CandidateProposition("wont_work", 4);

    CandidateProposition[] propositions = {
        newShouldBePersistedProposition1,
        newShouldBePersistedProposition2,
        newShouldBeIgnoredProposition,
        existingShouldBeUpdatedProposition,
        existingShouldBeIgnoredProposition
    };

    // when
    candidateService.evaluatePropositions(propositions);
    List<CandidateDocument> allCandidates = candidateRepository.findAll();
    Optional<CandidateDocument> shouldExistNow = candidateRepository.findByUrl("canon.com");
    Optional<CandidateDocument> existingUpdated = candidateRepository.findByUrl("shinjuku.com");
    Optional<CandidateDocument> existingUntouched = candidateRepository.findByUrl("akihabara.com");
    Optional<CandidateDocument> shouldNotExist = candidateRepository.findByUrl("wont_work");

    List<EvaluationTaskDocument> allTasks = evaluationRepository.findAll();
    int sumOfCandidatesWrittenOnTask = allTasks.get(0).getSumOfAllCandidates();

    // then
    assertThat(allCandidates.size()).isEqualTo(4);

    assertThat(shouldExistNow.isPresent()).isTrue();
    assertThat(existingUpdated.isPresent()).isTrue();
    assertThat(existingUntouched.isPresent()).isTrue();
    assertThat(shouldNotExist.isPresent()).isFalse();

    assertThat(shouldExistNow.get().getRank()).isEqualTo(342);
    assertThat(existingUpdated.get().getRank()).isEqualTo(6);
    assertThat(existingUntouched.get().getRank()).isEqualTo(2);

    assertThat(allTasks.size()).isEqualTo(1);
    assertThat(sumOfCandidatesWrittenOnTask).isEqualTo(4);

  }

}
