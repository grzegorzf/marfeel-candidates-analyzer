package com.forysiak.marfeelcandidatesanalyzer.domain.evaluation.model;

import com.forysiak.marfeelcandidatesanalyzer.domain.candidate.model.CandidateDocument;
import lombok.*;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Id;
import javax.persistence.Transient;
import java.time.LocalDate;
import java.util.List;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
@Document(collection = "EvaluationTask")
public class EvaluationTaskDocument {

  @Id
  private ObjectId id = new ObjectId();

  private LocalDate evaluatedAt;

  @DBRef
  private List<CandidateDocument> positiveCandidates;

  @DBRef
  private List<CandidateDocument> negativeCandidates;

  @DBRef
  private List<CandidateDocument> invalidCandidates;

  private long processingTime;

  @Transient
  public int getSumOfAllCandidates(){
    return this.positiveCandidates.size()
        + this.negativeCandidates.size()
        + this.invalidCandidates.size();
  }

}
