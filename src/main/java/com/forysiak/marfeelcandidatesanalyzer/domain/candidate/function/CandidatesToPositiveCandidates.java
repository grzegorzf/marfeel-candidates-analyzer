package com.forysiak.marfeelcandidatesanalyzer.domain.candidate.function;

import com.forysiak.marfeelcandidatesanalyzer.domain.candidate.model.CandidateDocument;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.forysiak.marfeelcandidatesanalyzer.domain.candidate.model.Qualification.POSITIVE;

@Component
public class CandidatesToPositiveCandidates implements Function<List<CandidateDocument>, List<CandidateDocument>> {
  @Override
  public List<CandidateDocument> apply(List<CandidateDocument> candidateDocuments) {
    return candidateDocuments.stream().filter(d -> POSITIVE.equals(d.getQualification())).collect(Collectors.toList());
  }
}
