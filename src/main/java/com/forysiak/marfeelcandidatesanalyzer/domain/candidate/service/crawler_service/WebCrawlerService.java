package com.forysiak.marfeelcandidatesanalyzer.domain.candidate.service.crawler_service;

import com.forysiak.marfeelcandidatesanalyzer.domain.candidate.function.UrlToFullUrl;
import com.forysiak.marfeelcandidatesanalyzer.domain.candidate.model.CandidateProposition;
import com.forysiak.marfeelcandidatesanalyzer.domain.candidate.model.Qualification;
import com.forysiak.marfeelcandidatesanalyzer.domain.candidate.predicate.IsDocumentMarfeelizable;
import com.forysiak.marfeelcandidatesanalyzer.domain.candidate.qualifier.MarfeelizableByTitle;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.stereotype.Service;

import java.io.IOException;

import static com.forysiak.marfeelcandidatesanalyzer.domain.candidate.model.Qualification.*;

@AllArgsConstructor
@Slf4j
@Service
public class WebCrawlerService implements CrawlerService {

  private final UrlToFullUrl urlToFullUrl;
  @MarfeelizableByTitle
  private final IsDocumentMarfeelizable isDocumentMarfeelizable;

  @Override
  public Qualification calculateQualification(CandidateProposition candidateProposition) {
    String fullUrl = urlToFullUrl.apply(candidateProposition.getUrl());
    try {
      Document document = getDocument(fullUrl);
      Qualification qualification = isDocumentMarfeelizable.test(document) ? POSITIVE : NEGATIVE;
      log.info("CrawlerService.calculateQualification {} for CandidateProposition: {} with qualification: {}", qualification, candidateProposition);
      return qualification;
    } catch (IOException e) {
      log.error("CrawlerService.calculateQualification INVALID for CandidateProposition: {}, reason: {}", candidateProposition, e.getMessage());
      return INVALID;
    }
  }

  @Override
  public Document getDocument(String fullUrl) throws IOException {
    return Jsoup.connect(fullUrl).get();
  }
}


