package com.forysiak.marfeelcandidatesanalyzer.domain.candidate.service.crawler_service;

import com.forysiak.marfeelcandidatesanalyzer.domain.candidate.model.CandidateProposition;
import com.forysiak.marfeelcandidatesanalyzer.domain.candidate.model.Qualification;
import org.jsoup.nodes.Document;

import java.io.IOException;

public interface CrawlerService {

  Qualification calculateQualification(CandidateProposition candidateProposition);

  Document getDocument(String fullUrl) throws IOException;

}
