package com.forysiak.marfeelcandidatesanalyzer.domain.evaluation.function;

import com.forysiak.marfeelcandidatesanalyzer.domain.candidate.model.CandidateDocument;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.function.Function;

@Component
@Slf4j
public class FutureToCandidateDocument implements Function<Future<CandidateDocument>, CandidateDocument> {
  @Override
  public CandidateDocument apply(Future<CandidateDocument> candidateDocumentFuture) {
    try {
      return candidateDocumentFuture.get();
    } catch (InterruptedException e) {
      log.error("InterruptedException for: {}, details: {} ", candidateDocumentFuture, e);
      return null;
    } catch (ExecutionException e) {
      log.error("ExecutionException for: {}, details: {} ", candidateDocumentFuture, e);
      return null;
    }
  }
}
