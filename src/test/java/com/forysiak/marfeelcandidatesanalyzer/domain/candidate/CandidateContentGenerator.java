package com.forysiak.marfeelcandidatesanalyzer.domain.candidate;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

class CandidateContentGenerator {

  private final static String HTML_BASE = "<html><head><title>%s</title></head><body></body></html>";

  static Document generateDocumentWithPositiveQualificationByTitle(){
    String html = String.format(HTML_BASE, "Some nice news ....");
    return Jsoup.parse(html);
  }

  static Document generateDocumentWithNegativeQualificationByTitle(){
    String html = String.format(HTML_BASE, "Some new things but not what u looking for.");
    return Jsoup.parse(html);
  }

}
